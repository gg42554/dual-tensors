import argparse
import os
import tensorflow as tf
import numpy as np
import pickle
from helpers import io_helper
from helpers import data_helper
from embeddings import text_embeddings
from embeddings import semrel_specific_embedder
from embeddings import semrel_trainer
from sys import stdin

parser = argparse.ArgumentParser(description='Trains a model for classification of asymmetric lexico-semantic relations.')
parser.add_argument('data', help='A path to the file containing training examples. Each training example should have the format: "first_concept \t second_concept \t label", where label is 0 or 1.')
parser.add_argument('embs', help='A path to the file containing pre-trained word embeddings')
parser.add_argument('output', help='A file path to which to store (serialize, pickle) the model.')
parser.add_argument('--slice', type=int, help='Number of tensor slices (not compatible with the bilinear product model, option -b)')
parser.add_argument('-s', '--single', action="store_true",  help='Use a single tensor model (instead of the default dual tensor model)')
parser.add_argument('-b', '--bilinear', action="store_true",  help='Use a simple bilinear product model (instead of the default dual tensor model). --slice is automatically set to 1 with this option.')

args = parser.parse_args()

if not os.path.isfile(args.data):
	print("Error: File with the training set not found.")
	exit(code = 1)

if not os.path.isfile(args.embs):
	print("Error: File containing word embeddings not found.")
	exit(code = 1)

if not os.path.isdir(os.path.dirname(args.output)) and not os.path.dirname(args.output) == "":
	print("Error: Output directory not found.")
	exit(code = 1)

num_slices = args.slice if args.slice is not None else 1
symmetric_relation = args.single
bilinear_product = args.bilinear

print("Loading dataset...")
train_set = io_helper.load_tab_separated_data(args.data)

print("Loading word embeddings...")
embeddings = text_embeddings.Embeddings()
embeddings.load_embeddings(args.embs, 200000, print_loading = True)
emb_size = embeddings.emb_sizes['en']

entity_dict = {}
dict_pairs = {}
print("Preparing training examples...")
train_data, sel_embs = data_helper.prepare_dataset_semrel_emb(entity_dict, [], [], embeddings, emb_size, train_set, dict_pairs)

model = semrel_specific_embedder.SemRel_Specific_Embedder(np.array(sel_embs, dtype = np.float32), emb_size, emb_size, num_slices, symmetric_relation = symmetric_relation, bilinear_product = bilinear_product)
if bilinear_product:
		model.no_embedding_specialization()
model.define_model(len(entity_dict), l2_reg_factor = 0.1)
model.define_optimization(learning_rate = 0.00001)

print("Initializing tensorflow session")
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

print("Training the model...")
semrel_trainer.train(sess, model, train_data, 50, 100, stop_num_epochs_not_better = 30)	

print("Serializing the model...")
pickle.dump(model.get_model(sess), open(args.output, "wb"))


