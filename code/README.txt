This document describes the code and data accompanying the EMNLP long paper submission titled: "Dual Tensor Model for Detecting Asymmetric Lexico-Semantic Relations". If you are using the model, please cite the following publication: 

@InProceedings{glavavsEtAl:2017:EMNLP,
  author    = {Glava\v{s}, Goran  and  Ponzetto, Simone Paolo},
  title     = {Dual Tensor Model for Detecting Asymmetric Lexico-Semantic Relations},
  booktitle = {Proceedings of the 2017 Conference on Empirical Methods in Natural Language Processing (EMNLP'17)},
  month     = {September},
  year      = {2017},
  address   = {Copenhagen, Denmark},
  publisher = {Association for Computational Linguistics},
  pages     = {in press}
}

The root directory contains two subdirectories:
1. data
2. code

The "data" directory contains subdirectories named following the dataset names in the paper. Each dataset folder contains corresponding portions of the dataset: 
1. train (except BLESS, Weeds, EVALuation, and Benotto)
2. validation 
3. test

The code folder contains the following two scripts that can be executed from the command line: 

1. dt-train.py trains the dual tensor model for classification of an asymmetric lexico-semantic relation. Mandatory arguments and options can be examined by running the command:
python dt-train.py -h

2. dt-eval.py evaluated the performance of a model (trained using dt-train.py) on a provided test set. Mandatory arguments and options can be examined by running the command:
python dt-eval.py -h

Using the above two scripts, it is possible to train and evaluate the following models described in the submitted paper: 

1. Dual-T
2. Single-T
3. Bilin-Prod

Prerequisites: 
1. Python scientific stack (numpy)
2. Tensorflow (version 1.0 or newer)

For questions please contact Goran Glavaš at: 
goran@informatik.uni-mannheim.de