from __future__ import division
import codecs
from os import listdir
from os.path import isfile, join
import pickle
import numpy as np

def load_embeddings_dict_with_norms(filepath, limit = 200000, special_tokens = None, print_load_progress = False):
	norms = []
	vocabulary = {}
	embeddings = []
	cnt = 0
	cnt_dict = 0
	with codecs.open(filepath,'r',encoding='utf8', errors='replace') as f:
		for line in f:
			try:
				cnt += 1
				if limit and cnt > limit: 
					break
				if print_load_progress and (cnt % 1000 == 0): 
					print("Loading embeddings: " + str(cnt))
				if cnt > 1:
					splt = line.split()
					word = splt[0]
					vec = [np.float32(x) for x in splt[1:]]
					
					vocabulary[word] = cnt_dict
					cnt_dict += 1
					norms.append(np.linalg.norm(vec, 2))
					embeddings.append(vec)
			
			except (IndexError, UnicodeEncodeError, ValueError):
				print("Incorrect format line!")
	
	if special_tokens is not None:
		for st in special_tokens:
			vocabulary[st] = cnt_dict
			cnt_dict += 1
			vec = np.random.uniform(-1.0, 1.0, size = [len(embeddings[0])])
			norms.append(np.linalg.norm(vec, 2))
			embeddings.append(vec)

	return vocabulary, np.array(embeddings, dtype = np.float32), norms 

def load_whitespace_separated_data(filepath):
	lines = list(codecs.open(filepath,'r',encoding='utf8', errors='replace').readlines())
	return [l.strip().split() for l in lines]

def load_tab_separated_data(filepath):
	lines = list(codecs.open(filepath,'r',encoding='utf8', errors='replace').readlines())
	return [l.strip().split('\t') for l in lines]

def load_wn_concepts_dict(path):
	lines = list(codecs.open(path,'r',encoding='utf8', errors='replace').readlines())
	lcols = {x[0] : ' '.join((x[1].split('_'))[2:-2]) for x in [l.strip().split() for l in lines]}
	return lcols

def load_bless_dataset(path):
	lines = list(codecs.open(path,'r',encoding='utf8', errors='replace').readlines())
	lcols = [(x[0].split('-')[0], x[3].split('-')[0], "1" if x[2] == "hyper" else "0") for x in [l.strip().split() for l in lines]]
	return lcols

def write_list(path, list):
	f = codecs.open(path,'w',encoding='utf8')
	f.writelines(list)
	f.close()

def load_translation_pairs(filepath):
	lines = list(codecs.open(filepath,'r',encoding='utf8', errors='replace').readlines())
	dataset = []; 
	for line in lines:
		spl = line.split(',')
		srcword = spl[0].strip()
		trgword = spl[1].strip(); 
		if (" " not in srcword.strip()) and  (" " not in trgword.strip()):
			dataset.append((srcword, trgword)); 
	return dataset	

def write_list_tuples_separated(path, list, delimiter = '\t'):
	f = codecs.open(path,'w',encoding='utf8')
	for i in range(len(list)):
		for j in range(len(list[i])):
			if j == len(list[i]) - 1: 
				f.write(str(list[i][j]) + '\n')
			else:
				f.write(str(list[i][j]) + delimiter)  
	f.close()

def store_wordnet_rels(dirpath, relname, pos, lang, instances):
	f = codecs.open(dirpath + "/" + lang + "_" + relname + "_" + pos + ".txt",'w',encoding='utf8')
	for i in instances:
		splt = i.split('::')
		f.write(splt[0].replace("_", " ") + "\t" + splt[1].replace("_", " ") + "\t" + str(instances[i]) + "\n")
	f.close()

def load_csv_lines(path, delimiter = ',', indices = None):
	f = codecs.open(path,'r',encoding='utf8')
	lines = [l.strip().split(delimiter) for l in f.readlines()]
	if indices is None:
		return lines
	else:
		return [sublist(l, indices) for l in lines]

def load_csv_lines_line_by_line(path, delimiter = ',', indices = None, limit = None):
	lines = []
	f = codecs.open(path,'r',encoding='utf8')
	line = f.readline().strip()
	cnt = 1
	while line is not None:
		lines.append(sublist(line, indices) if indices is not None else line.split(delimiter))	
		line = f.readline().strip()
		cnt += 1
		if limit is not None and cnt > limit:
			break
	return lines

def sublist(list, indices):
	sublist = []
	for i in indices:	
		sublist.append(list[i])
	return sublist
		