import argparse
import os
import tensorflow as tf
import numpy as np
import pickle
from helpers import io_helper
from helpers import data_helper
from embeddings import text_embeddings
from embeddings import semrel_specific_embedder
from embeddings import semrel_trainer

parser = argparse.ArgumentParser(description='Predicts and existence of an asymmetric lexico-semantic relation between pairs of concepts/words, using a previously trained classification model.')
parser.add_argument('data', help='A path to the file containing prediction examples (test set). Each test example should have the format: "first_concept TAB second_concept TAB label", where label is 0 or 1.')
parser.add_argument('model', help='A path to the file containing the serialized model.')
parser.add_argument('embs', help='A path to the file containing pre-trained word embeddings')
parser.add_argument('--preds', help='A file path to which to store predictions of the model for the test set pairs.')
parser.add_argument('-b', '--bilinear', action="store_true",  help='Indicates whether the loaded model is a simple bilinear product model.')

args = parser.parse_args()

if not os.path.isfile(args.data):
	print("Error: File with the test set not found.")
	exit(code = 1)

if not os.path.isfile(args.model):
	print("Error: File containig a serialized model not found.")
	exit(code = 1)

if not os.path.isfile(args.embs):
	print("Error: File containig a serialized model not found.")
	exit(code = 1)

if args.preds is not None and not os.path.isdir(os.path.dirname(args.preds)) and not os.path.dirname(args.preds) == "":
	print("Error: Predictions output directory not found.")
	exit(code = 1)


###### Loading classifier #############
print("Loading model")
classifier = semrel_specific_embedder.load_classifier(args.model, just_bilin_prod = args.bilinear)

print("Loading dataset...")
test_set = io_helper.load_tab_separated_data(args.data)

print("Loading word embeddings...")
embeddings = text_embeddings.Embeddings()
embeddings.load_embeddings(args.embs, 200000, print_loading = True)

print("Initializing tensorflow session...")
session = tf.InteractiveSession()
session.run(tf.global_variables_initializer())

###### preparing test set for predictions #######
print("Preparing test examples...")
left_mat, right_mat, eval_labels, words = data_helper.prepare_eval_semrel_emb(embeddings, [], classifier.emb_size, test_set, keep_words = True)
	
###### predicting and evaluating ################
print("Computing predictions...")
preds = semrel_specific_embedder.get_predictions(session = session, model = classifier, left_input = np.array(left_mat, dtype = np.float32), right_input = np.array(right_mat, dtype = np.float32))
semrel_trainer.eval(preds, eval_labels)

if args.preds is not None:
	print("Writing predictions to file...")
	to_write = list(zip(words, preds, eval_labels))
	io_helper.write_list_tuples_separated(args.preds, to_write)

print("All done, ciao bella!")


