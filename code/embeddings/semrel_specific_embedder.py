import tensorflow as tf
import numpy as np
import pickle
def load_classifier(path, just_bilin_prod = False):
	model = pickle.load(open(path, "rb" ))
	config = model[0]
	print("Config: " + str(config))
	parameters = model[1:]
	classifier = SemRel_Specific_Embedder(None, emb_size = config[0], target_emb_size =  config[1], num_slices = config[2], symmetric_relation = config[3], bilinear_product = config[4])  
	if just_bilin_prod:
		classifier.no_embedding_specialization()
	classifier.load_trained_model(parameters)
	return classifier
def get_asymmetric_vecs(session, model, left_input, right_input):
	lvecs = model.left_reps.eval(session = session, feed_dict = { model.input_w1 : left_input })
	rvecs = model.right_reps.eval(session = session, feed_dict = { model.input_w2 : right_input })
	return lvecs, rvecs
def get_predictions(session, model, left_input, right_input):
	preds = model.predictions.eval(session = session, feed_dict = { model.input_w1 : left_input, model.input_w2 : right_input })
	return preds
	
class SemRel_Specific_Embedder(object):
	"""
	A model for deriving word embeddings that capture a specific semantic relation. 
	"""
	def no_embedding_specialization(self):
		self.specialize_embeddings = False
	def __init__(self, word_embeddings, emb_size = 100, target_emb_size = 100, num_slices = 1, symmetric_relation = True, bilinear_product = True):
		self.ent_emb_dense = word_embeddings
		self.emb_size = emb_size
		self.symmetric_rel = symmetric_relation
		self.target_emb_size = target_emb_size
		self.num_slices = num_slices
		self.bilinear_product = bilinear_product
		self.specialize_embeddings = True
	
	def load_trained_model(self, parameters):	
		self.input_w1 = tf.placeholder(tf.float32, [None, self.emb_size], name="w1")
		self.input_w2 = tf.placeholder(tf.float32, [None, self.emb_size], name="w2")
		self.rel_W = tf.Variable(parameters[0], dtype = tf.float32, name = "rel_W")
		self.rel_b = tf.Variable(parameters[1], dtype = tf.float32, name = "rel_b")
		if not self.symmetric_rel:
			self.inv_rel_W = tf.Variable(parameters[2], dtype = tf.float32, name = "inv_rel_W")
			self.inv_rel_b = tf.Variable(parameters[3], dtype = tf.float32, name = "inv_rel_b")
			if self.bilinear_product:
				self.bilinear_W = tf.Variable(parameters[4], dtype = tf.float32, name = "bilinear_W")
		elif self.bilinear_product:	
			self.bilinear_W = tf.Variable(parameters[2], dtype = tf.float32, name = "bilinear_W")
				
		self.first_embs_extended = tf.tile(tf.expand_dims(self.input_w1, axis = 0), [self.num_slices, 1, 1])
		self.second_embs_extended = tf.tile(tf.expand_dims(self.input_w2, axis = 0), [self.num_slices, 1, 1])
		self.first_transformed = tf.nn.tanh(tf.add(tf.matmul(self.first_embs_extended, self.rel_W), self.rel_b))
		if self.symmetric_rel:
			self.second_transformed = tf.nn.tanh(tf.add(tf.matmul(self.second_embs_extended, self.rel_W), self.rel_b))
		else:
			self.second_transformed = tf.nn.tanh(tf.add(tf.matmul(self.second_embs_extended, self.inv_rel_W), self.inv_rel_b))
		self.left_reps = tf.reshape(tf.transpose(self.first_transformed, perm=[1, 0, 2]), [-1, self.num_slices * self.target_emb_size])  			
		self.right_reps = tf.reshape(tf.transpose(self.second_transformed, perm=[1, 0, 2]), [-1, self.num_slices * self.target_emb_size])
		# bilinear products between pairs of transformed vectors
		if self.bilinear_product:
			if self.specialize_embeddings:
				lin_part = tf.matmul(self.first_transformed, self.bilinear_W)
				bilin_part = tf.multiply(lin_part, self.second_transformed)
			else:
				print("Simple bilinear product without embedding specialization!")	
				lin_part = tf.matmul(self.first_embs_extended, self.bilinear_W)
				bilin_part = tf.multiply(lin_part, self.second_embs_extended)
			self.predictions = tf.reduce_mean(tf.nn.tanh(tf.reduce_sum(bilin_part, axis = -1)), axis = 0)
		# cosine between transformed vectors
		else:
			dotprod = tf.reduce_sum(tf.multiply(self.left_reps, self.right_reps), axis = 1)
			norms_product = tf.sqrt(tf.reduce_sum(tf.square(self.left_reps), axis = 1)) * tf.sqrt(tf.reduce_sum(tf.square(self.right_reps), axis = 1))
			self.predictions = tf.div(dotprod, norms_product)
			
	def define_model(self, num_entities, l2_reg_factor = 0.0):
		self.input_w1 = tf.placeholder(tf.int32, [None,], name="w1")
		self.input_w2 = tf.placeholder(tf.int32, [None,], name="w2")
		self.input_y = tf.placeholder(tf.float32, [None,], name="y")
		self.dropout = tf.placeholder(tf.float32, name="dropout")
		if self.ent_emb_dense is None:
			self.ent_emb_dense = np.random.uniform(-1.0, 1.0, size = [num_entities, self.emb_size]).astype(np.float32)
		self.word_embeddings = tf.constant(self.ent_emb_dense, name="word_embs")
		self.rel_W = tf.get_variable("rel_W", shape=[self.num_slices, self.emb_size, self.target_emb_size], initializer=tf.contrib.layers.xavier_initializer())
		self.rel_b = tf.get_variable("rel_b", shape=[self.num_slices, 1, self.target_emb_size], initializer=tf.contrib.layers.xavier_initializer())
		if not self.symmetric_rel:
			self.inv_rel_W = tf.get_variable("inv_rel_W", shape=[self.num_slices, self.emb_size, self.target_emb_size], initializer=tf.contrib.layers.xavier_initializer())
			self.inv_rel_b = tf.get_variable("inv_rel_b", shape=[self.num_slices, 1, self.target_emb_size], initializer=tf.contrib.layers.xavier_initializer())
			
		if self.bilinear_product:
			self.bilinear_W = tf.get_variable("bilinear_W", shape=[self.num_slices, self.target_emb_size, self.target_emb_size], initializer=tf.contrib.layers.xavier_initializer())
		self.first_word_embeddings = tf.nn.embedding_lookup(self.word_embeddings, self.input_w1)
		self.second_word_embeddings = tf.nn.embedding_lookup(self.word_embeddings, self.input_w2)
		
		self.first_embs_extended = tf.tile(tf.expand_dims(self.first_word_embeddings, axis = 0), [self.num_slices, 1, 1])
		self.second_embs_extended = tf.tile(tf.expand_dims(self.second_word_embeddings, axis = 0), [self.num_slices, 1, 1])
		self.first_transformed = tf.nn.tanh(tf.add(tf.matmul(self.first_embs_extended, self.rel_W), self.rel_b))
		if self.symmetric_rel:
			self.second_transformed = tf.nn.tanh(tf.add(tf.matmul(self.second_embs_extended, self.rel_W), self.rel_b))
		else:
			self.second_transformed = tf.nn.tanh(tf.add(tf.matmul(self.second_embs_extended, self.inv_rel_W), self.inv_rel_b))
		l2_loss = tf.constant(0.0)
		if self.specialize_embeddings:
			l2_loss += tf.nn.l2_loss(self.rel_W)
			l2_loss += tf.nn.l2_loss(self.rel_b)
		if not self.symmetric_rel:
			l2_loss += tf.nn.l2_loss(self.inv_rel_W)
			l2_loss += tf.nn.l2_loss(self.inv_rel_b)
		
		if self.bilinear_product:
			l2_loss += tf.nn.l2_loss(self.bilinear_W)
		self.left_reps = tf.reshape(tf.transpose(self.first_transformed, perm=[1, 0, 2]), [-1, self.num_slices * self.target_emb_size])  			
		self.right_reps = tf.reshape(tf.transpose(self.second_transformed, perm=[1, 0, 2]), [-1, self.num_slices * self.target_emb_size])
		# bilinear products between pairs of transformed vectors
		if self.bilinear_product:
			if self.specialize_embeddings:
				lin_part = tf.matmul(self.first_transformed, self.bilinear_W)
				bilin_part = tf.multiply(lin_part, self.second_transformed)
			else:
				print("Simple bilinear product without embedding specialization!")	
				lin_part = tf.matmul(self.first_embs_extended, self.bilinear_W)
				bilin_part = tf.multiply(lin_part, self.second_embs_extended)
			self.predictions = tf.reduce_mean(tf.nn.tanh(tf.reduce_sum(bilin_part, axis = -1)), axis = 0)
		# cosine between transformed vectors
		else: 
			dotprod = tf.reduce_sum(tf.multiply(self.left_reps, self.right_reps), axis = 1)
			norms_product = tf.sqrt(tf.reduce_sum(tf.square(self.left_reps), axis = 1)) * tf.sqrt(tf.reduce_sum(tf.square(self.right_reps), axis = 1))
			self.predictions = tf.div(dotprod, norms_product)
						
		self.pure_loss = tf.reduce_sum(tf.maximum(tf.subtract(1.0, tf.multiply(self.predictions, self.input_y)), 0.0))
		self.loss = self.pure_loss + l2_reg_factor * l2_loss
			
	def define_optimization(self, learning_rate = 1e-5):
		self.train_step = tf.train.RMSPropOptimizer(learning_rate).minimize(self.loss)
	def train(self, session, w1_indices, w2_indices, y_vals, compute_loss = False, dropout_rate = 0.5):
		fd = { self.input_w1: w1_indices, self.input_w2: w2_indices, self.input_y : y_vals, self.dropout : dropout_rate }
		self.train_step.run(session = session, feed_dict = fd)
		
		if compute_loss:
			loss_val = self.pure_loss.eval(session = session, feed_dict = fd)
			return loss_val
	def predict(self, session, w1_indices, w2_indices, y_vals = None):
		fd = { self.input_w1: w1_indices, self.input_w2: w2_indices, self.dropout : 1.0 }
		if y_vals is not None:
			fd.update({self.input_y : y_vals})
		preds = self.predictions.eval(session = session, feed_dict = fd)
		gold = y_vals 	
		
		if y_vals is not None: 
			loss = self.pure_loss.eval(session = session, feed_dict = fd)
			return preds, gold, loss
		else:
			return preds
	def get_model(self, session):
		r_W = self.rel_W.eval(session = session)
		r_b = self.rel_b.eval(session = session)
		if not self.symmetric_rel:
			inv_r_W = self.inv_rel_W.eval(session = session)
			inv_r_b = self.inv_rel_b.eval(session = session)
		if self.bilinear_product:
			bl_W = self.bilinear_W.eval(session = session)
		config = (self.emb_size, self.target_emb_size, self.num_slices, self.symmetric_rel, self.bilinear_product)
		if self.symmetric_rel:
			if self.bilinear_product:
				model = [config, r_W, r_b, bl_W]
			else:
				model = [config, r_W, r_b]
		else:
			if self.bilinear_product:
				model = [config, r_W, r_b, inv_r_W, inv_r_b, bl_W]
			else:
				model = [config, r_W, r_b, inv_r_W, inv_r_b]
		return model