import numpy as np
import helpers.io_helper as ioh

def aggregate_phrase_embedding(words, stopwords, embs, emb_size, l2_norm_vec = True, lang = 'en'):
	vec_res = np.zeros(emb_size)
	fit_words = [w.lower() for w in words if w.lower() not in stopwords and w.lower() in embs.lang_vocabularies[lang]]
	if len(fit_words) == 0:
		return None

	for w in fit_words:
		vec_res += embs.get_vector(lang, w) 	
	res = np.multiply(1.0 / (float(len(fit_words))), vec_res)
	if l2_norm_vec:
		res = np.multiply(1.0 / np.linalg.norm(res), res)
	return res


class Embeddings(object):
	"""Captures functionality to load and store textual embeddings"""

	def __init__(self, cache_similarities = False):
		self.lang_embeddings = {}
		self.lang_emb_norms = {}
		self.lang_vocabularies = {}
		self.emb_sizes = {}
		self.cache = {}
		self.do_cache = cache_similarities

	def get_vector(self, lang, word):
		if word in self.lang_vocabularies[lang]:
			return self.lang_embeddings[lang][self.lang_vocabularies[lang][word]]
		else: 
			return None

	def set_vector(self, lang, word, vector):
		if word in self.lang_vocabularies[lang]:
			self.lang_embeddings[lang][self.lang_vocabularies[lang][word]] = vector

	def get_norm(self, lang, word):
		if word in self.lang_vocabularies[lang]:
			return self.lang_emb_norms[lang][self.lang_vocabularies[lang][word]]
		else: 
			return None

	def set_norm(self, lang, word, norm):
		if word in self.lang_vocabularies[lang]:
			self.lang_emb_norms[lang][self.lang_vocabularies[lang][word]] = norm

	def remove_word(self, lang, word):
		self.lang_vocabularies[self.lang].pop(word, None)
	
	def load_embeddings(self, filepath, limit, language = 'en', print_loading = "False"):
		vocabulary, embs, norms = ioh.load_embeddings_dict_with_norms(filepath, limit = limit, special_tokens = ["<PAD/>"], print_load_progress = print_loading)		
		self.lang_embeddings[language] = embs
		self.lang_emb_norms[language] = norms
		self.emb_sizes[language] = embs.shape[1]
		self.lang_vocabularies[language] = vocabulary	
	

	def word_similarity(self, first_word, second_word, first_language = 'en', second_language = 'en'):	
		if self.do_cache:
			cache_str = min(first_word, second_word) + "-" + max(first_word, second_word)
			if (first_language + "-" + second_language) in self.cache and cache_str in self.cache[first_language + "-" + second_language]:
				return self.cache[first_language + "-" + second_language][cache_str]
		elif (first_word not in self.lang_vocabularies[first_language] and first_word.lower() not in self.lang_vocabularies[first_language]) or (second_word not in self.lang_vocabularies[second_language] and second_word.lower() not in self.lang_vocabularies[second_language]):
			if ((first_word in second_word or second_word in first_word) and first_language == second_language) or first_word == second_word:
					return 1
			else:
					return 0

		index_first = self.lang_vocabularies[first_language][first_word] if first_word in self.lang_vocabularies[first_language] else self.lang_vocabularies[first_language][first_word.lower()]
		index_second = self.lang_vocabularies[second_language][second_word] if second_word in self.lang_vocabularies[second_language] else self.lang_vocabularies[second_language][second_word.lower()]		
		
		first_emb = self.lang_embeddings[first_language][index_first]
		second_emb = self.lang_embeddings[second_language][index_second] 

		first_norm = self.lang_emb_norms[first_language][index_first]
		second_norm = self.lang_emb_norms[second_language][index_second]

		score =  np.dot(first_emb, second_emb) / (first_norm * second_norm)
		if self.do_cache:
			if (first_language + "-" + second_language) not in self.cache:
				self.cache[first_language + "-" + second_language] = {}
				if cache_str not in self.cache[first_language + "-" + second_language]:
					self.cache[first_language + "-" + second_language][cache_str] = score		
		return score