from ml import batcher
import numpy as np
from evaluation import retrieval
from helpers import io_helper
from sklearn import linear_model

def train_and_test_baseline(session, model, train_data, test_data, c = 1):
	w1_indices_train, w2_indices_train, y_vals_train = zip(*train_data)
	w1_indices_test, w2_indices_test, y_vals_test = zip(*test_data)

	fd = { model.input_w1: w1_indices_train, model.input_w2: w2_indices_train}
	first_vecs = model.first_word_embeddings.eval(session = session, feed_dict = fd)	
	second_vecs = model.second_word_embeddings.eval(session = session, feed_dict = fd)
	x_vals_train = np.concatenate((first_vecs, second_vecs), axis = 1)
	
	lr = linear_model.LogisticRegression(C = c)
	lr.fit(x_vals_train, y_vals_train)

	fd = { model.input_w1: w1_indices_test, model.input_w2: w2_indices_test}
	first_vecs = model.first_word_embeddings.eval(session = session, feed_dict = fd)	
	second_vecs = model.second_word_embeddings.eval(session = session, feed_dict = fd)
	x_vals_test = np.concatenate((first_vecs, second_vecs), axis = 1)
	y_pred = lr.predict(x_vals_test)

	print("Logistic regression baseline: ")
	eval(y_pred, y_vals_test)

def train(session, model, train_data, batch_size, num_batches_print = 50, stop_num_epochs_not_better = 50):
	batches = batcher.batch_iter(train_data, batch_size, 5000, shuffle = True)
	num_batches_per_epoch = int(len(train_data)/batch_size) + 1

	batch_counter = 0
	epoch_counter = 0
	last_epochs = []
	epoch_loss = 0.0
	
	for batch in batches:
		w1_indices_train, w2_indices_train, y_vals_train = zip(*batch)

		batch_loss = model.train(session, w1_indices_train, w2_indices_train, y_vals_train, compute_loss = True, dropout_rate = 0.5)
		epoch_loss += batch_loss

		if batch_counter % num_batches_print == 0:
			print("Training (" + str(batch_counter) + " batches)")
			
		if batch_counter % num_batches_per_epoch == 0 and batch_counter > 0:		 		
			epoch_counter += 1
			print("Epoch " + str(epoch_counter) + " completed.")
			
			instance_loss = epoch_loss / (num_batches_per_epoch * batch_size)
			last_epochs.append(instance_loss)
			epoch_loss = 0.0
			
			if len(last_epochs) > stop_num_epochs_not_better:
				last_epochs.pop(0)
			print("Last epochs: " + str(last_epochs))

			if len(last_epochs) == stop_num_epochs_not_better and last_epochs[0] < last_epochs[-1]:
				print("End condition satisfied, training finished. ")
				break
		batch_counter += 1			
				
		

def train_and_test(session, model, train_data, test_data, batch_size, num_epochs, num_batches_print = 50, dropout_rate = 0.5, stop_num_epochs_not_better = 50, report_num_batch = 10000, report_file = None):
	w1_indices_test, w2_indices_test, y_vals_test = zip(*test_data)
	
	batches = batcher.batch_iter(train_data, batch_size, num_epochs, shuffle = True)
	num_batches_per_epoch = int(len(train_data)/batch_size) + 1

	batch_counter = 0
	epoch_counter = 0

	best_model = None
	best_score = 0.0
	last_epochs = []	

	epoch_loss = 0.0
	for batch in batches:
		w1_indices_train, w2_indices_train, y_vals_train = zip(*batch)

		batch_loss = model.train(session, w1_indices_train, w2_indices_train, y_vals_train, compute_loss = True, dropout_rate = dropout_rate)
		epoch_loss += batch_loss		

		if batch_counter % report_num_batch == 0 and report_file is not None:
			if batch_counter > 0:
				io_helper.write_list(report_file, [str(x) + "\n" for x in last_batch_scores_losses])
			last_batch_scores_losses = []
			last_batch_scores_performance = []

		if batch_counter % num_batches_print == 0 or batch_counter % num_batches_per_epoch == 0:
			preds, gold, loss = model.predict(session, w1_indices_test, w2_indices_test, y_vals_test)
			print("After " + str(batch_counter) + " batches: ")
			loss_instance_test = loss / float(len(w1_indices_test))
			print("Margin loss (test, avg per example): " + str(loss_instance_test))
			score = eval(preds, gold)

			if (score > best_score):
				best_model = model.get_model(session)
				best_score = score
			
			if (report_file is not None):
				last_batch_scores_losses.append(loss_instance_test)
				last_batch_scores_performance.append(score)
		
			
		if batch_counter % num_batches_per_epoch == 0:		 		
			preds, gold, loss = model.predict(session, w1_indices_test, w2_indices_test, y_vals_test)
			loss_instance_test = loss / float(len(w1_indices_test))
			
			last_epochs.append(score)
			if len(last_epochs) > stop_num_epochs_not_better:
				last_epochs.pop(0)
			print("Last epochs: " + str(last_epochs))
			if len(last_epochs) == stop_num_epochs_not_better and last_epochs[0] > last_epochs[-1]:
				print("End condition satisfied, exiting training. ")
				break

			epoch_counter += 1
			print("\n\nAfter " + str(batch_counter) + " batches (" + str(epoch_counter) + " epochs): ")
			print("Epoch loss (train, per example): " + str(epoch_loss / float(len(train_data))) + "\n")
			epoch_loss = 0.0

		batch_counter += 1

	return best_model, best_score

def eval(preds, gold):
	preds_binary = [(-1 if x < 0 else 1) for x in preds]
	num_ones_preds = sum([x for x in preds_binary if x == 1])
	print("# pos. preds: " + str(num_ones_preds))

	pairs = list(zip(preds_binary, gold))
	tp = sum([(1 if x == (1, 1) else 0) for x in pairs])
	fp = sum([(1 if x == (1, -1) else 0) for x in pairs])
	fn = sum([(1 if x == (-1, 1) else 0) for x in pairs])
	tn = sum([(1 if x == (-1, -1) else 0) for x in pairs])
	
	precision = float(tp) / float(tp + fp) if tp + fp > 0 else 0
	recall = float(tp) / float(tp + fn) if tp + fn > 0 else 0
	accuracy = float(tp + tn) / float(len(pairs))
	f1 = (2 * precision * recall) / (precision + recall) if (precision + recall > 0) else 0
	ap = retrieval.average_precision(preds, gold, limit = None)
	ap100 = retrieval.average_precision(preds, gold, limit = 100)

	print("Accuracy (test): " + str(accuracy))
	print("P, R, F (test): " + str(precision) + "; " + str(recall) + "; " + str(f1))
	print("AP: " + str(ap) + "; AP100: " + str(ap100))	

	return ap
	

